# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/devcs/catkin_ws_turtleBot/src/turtlebot3_simulations/turtlebot3_fake/include".split(';') if "/home/devcs/catkin_ws_turtleBot/src/turtlebot3_simulations/turtlebot3_fake/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;std_msgs;sensor_msgs;geometry_msgs;nav_msgs;tf;turtlebot3_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "turtlebot3_fake"
PROJECT_SPACE_DIR = "/home/devcs/catkin_ws_turtleBot/devel"
PROJECT_VERSION = "1.1.0"
